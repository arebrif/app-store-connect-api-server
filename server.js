const port = process.env.PORT || 4000;

const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

dotenv.config();

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/*', function(req, res, next) {
    // do your filtering here, call a `res` method if you want to stop progress or call `next` to proceed
    let ip = req.ip || 
                req.headers['x-forwarded-for'] || 
                req.connection.remoteAddress || 
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress;

    console.log('Request ip:', ip);
    next();
});

app.get('/', (req, res) => res.send('App Store Connect API server is running'));

app.post('/auth', (req, res) => {
    // console.log('Request:', req);
    // console.log('Request headers:', req.headers);
    // console.log('Request headers user agent:', req.headers["user-agent"]);
    
    const fs = require('fs')

    fs.readFile('./keys/' + process.env.ASC_API_KEY, 'utf8' , (err, privateKey) => {
        if (err) {
            console.error(err)
            return
        }

        let exp = Math.round((new Date()).getTime() / 1000) + 1199;

        let payload = {
            "iss": process.env.ISSUER_ID,
            "exp": exp,
            "aud": "appstoreconnect-v1"
        };
        let options = {
            "algorithm": "ES256",
            header: {
                "alg": "ES256",
                "kid": process.env.KEY_ID,
                "typ": "JWT"
            }
        };

        jwt.sign(payload, privateKey, options, function(err, token) {
            if (err) {
                res.json({ error: err });
            } else {
                console.log(token);
                process.env['ASC_API_TOKEN'] = token;
                console.log('token', process.env.ASC_API_TOKEN);
                res.json({ token: token });
            }
        });
    })
});

app.get('/users', (req, res) => {
    const https = require('https')
    const options = {
        hostname: 'api.appstoreconnect.apple.com',
        // port: 443,
        path: '/v1/users',
        method: 'GET',
        headers: {
            'Authorization': ' Bearer ' + process.env.ASC_API_TOKEN
        }
    }

    console.log('token', process.env.ASC_API_TOKEN);

    const req1 = https.request(options, res1 => {
        console.log(`statusCode: ${res1.statusCode}`)

        var body = '';

        res1.on('data', d => {
            process.stdout.write(d)
            body += d
        })

        res1.on('end', function() {
            var json = JSON.parse(body)
            console.log("Got a response: ", json)
            res.json(json)
        });
    })

    req1.on('error', error => {
        console.error(error)
        res.send('Error' + error)
    })

    req1.end()
});

app.get('/apps', (req, res) => {
    const https = require('https')
    const options = {
        hostname: 'api.appstoreconnect.apple.com',
        path: '/v1/apps?fields[apps]=name,bundleId,builds',
        method: 'GET',
        headers: {
            'Authorization': ' Bearer ' + process.env.ASC_API_TOKEN
        }
    }

    console.log('token', process.env.ASC_API_TOKEN);

    const req1 = https.request(options, res1 => {
        console.log(`statusCode: ${res1.statusCode}`)

        var body = '';

        res1.on('data', d => {
            process.stdout.write(d)
            body += d
        })

        res1.on('end', function() {
            var json = JSON.parse(body)
            console.log("Got a response: ", json)
            res.json(json)
        });
    })

    req1.on('error', error => {
        console.error(error)
        res.send('Error' + error)
    })

    req1.end()
});

app.get('/posts', (req, res) => {
    var file = require("./resources/posts.json");
    res.json(file);
});

app.listen(port, () => console.log(`Test server is listening on port ${port}!`));